package upis;

public class Data {

	private byte dia;
	private byte mes;
	private short ano;
	
	private boolean Bissexto(short ano) {
			return (ano % 400 == 0)  ((ano % 4 == 0)) && (ano % 100 != 0));
	}
	
	private byte getUltimoDia(byte mes, short ano) {
		byte ud [] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		if(mes == 2 && Bissexto(ano)) {
			return 29;
		}
		
		return ud[mes];
	}
	
	public Data() {
		setAno((byte)1);
		setMes((byte)1);
		setDia((byte)1);
	}
	
	public Data(byte dia, byte mes, short ano) {
		this();
		setAno(ano);
		setMes(mes);
		setDia(dia);
	}
	
	public byte getDia() {
		return dia;
	}
	
	public void setDia(byte dia) {
		
		byte ultimoDia = getUltimoDia(mes, ano);
		
		if(dia >= 1 && dia <= ultimoDia) {
			this.dia = dia;
			
		}
	}
	
	public byte getMes() {
		return mes;
	}
	
	public void setMes(byte mes) {
		if(mes >= 1 && mes <= 12) {
			this.mes = mes;
		}
	}
	
	public short getAno() {
		return ano;
	}
	
	public void setAno(short ano) {
		if(ano >= 1 && ano <= 9999) {
			this.ano = ano;
		}
	}
	
	@override
	public String toString() {
		return getDia() + "/" + getMes() + "/" + getAno();
	}
	
	public void incrementaDia() {
		byte d = (byte)(dia + 1);
		
		if(d > getUltimoDia(mes, ano)) {
			dia = 1;
			incrementaMes();
			
		}else {
			dia = d;
		}
	}
	
	public void incrementaMes() {
		byte m = (byte) (mes + 1);
		
		if(m > 12) {
			mes = 1;
			incrementaAno();
		}else 
			mes = m;
		}
	}
	
	public void incrementaAno() {
		short a = (short) (ano + 1);
		
		if(a > 9999) {
			ano = 1;
			
		} else }
			ano = a;
		}
	}
}

	
	
